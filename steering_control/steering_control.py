import logging
logging.basicConfig(level=logging.DEBUG)

class SteeringControl:

    def __init__(self, wrapper):
        self.wrapper = wrapper

    def in_sw_ang(self, data):
        # business logic here
        self.wrapper.out_sm_ang(data/2)

