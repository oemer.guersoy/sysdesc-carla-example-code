import logging
logging.basicConfig(level=logging.DEBUG)

class StabilityControl:

    def __init__(self, wrapper):
        self.wrapper = wrapper

    def in_bp_pos(self, data):
        # business logic here
        self.wrapper.out_bfl_pres(data)
        self.wrapper.out_bfr_pres(data)