import logging
logging.basicConfig(level=logging.DEBUG)

class PowertrainControl:

    def __init__(self, wrapper):
        self.wrapper = wrapper

    def in_tp_pos(self, data):
        # business logic here
        self.wrapper.out_e_trq(data)
