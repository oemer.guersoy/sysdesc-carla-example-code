import logging
logging.basicConfig(level=logging.DEBUG)

import pygame

import os
os.environ["SDL_VIDEODRIVER"] = "dummy"

import time
import threading

class ActuationSensors:

    def __init__(self, wrapper):
        self.wrapper = wrapper
        
        pygame.init()
        self._js = pygame.joystick.Joystick(0)
        self._js.init()
        
        self.run()

    def run(self):
        
        while True:
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
    
            numAxes = self._js.get_numaxes()
            jsAxisInputs = [ float(self._js.get_axis(i)) for i in range(numAxes)]
            numButtons = self._js.get_numbuttons()
            jsButtonInputs = [ float(self._js.get_button(i)) for i in range(numButtons)]
    
            brake = 0.0 if jsAxisInputs[3] <= 0 else jsAxisInputs[3]
            throttle =  0.0 if jsAxisInputs[4] <= 0 else jsAxisInputs[4]
            steer = 0.0 if jsAxisInputs[0] == 0 else jsAxisInputs[0]
            
            self.wrapper.out_bp_pos(brake)
            self.wrapper.out_tp_pos(throttle)
            self.wrapper.out_sw_ang(steer)

            time.sleep(0.1)

